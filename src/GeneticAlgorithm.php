<?php

namespace App;

use App\GeneticAlgorithm\Population;
use JetBrains\PhpStorm\NoReturn;
use function Termwind\{render, ask};

class GeneticAlgorithm
{
    public function __construct(
        protected int $maxExecutionTime = 16789,
    ) {}

    #[NoReturn]
    public function run(): void
    {
        $population = new Population($this->getTargetGene());

        for ($time = 1; $time <= $this->maxExecutionTime; $time++) {
            $best = reset($population->population);
            $bestGene = $best->gene;
            render("<p class='m-0'>➡ Generation : $time : <b>$bestGene</b></p>");

            if ($best->fitness === 0) {
                render('<h2 class="mt-1 bg-green p-5">Best match found</h2>');
                return;
            }

            $population->evolve();
        }

        render('<h2 class="mt-1 bg-danger p-5">Unable to find the best match</h2>');
    }

    private function getTargetGene(): string
    {
        return ask(<<<HTML
                <span class="mr-1 bg-green px-1 text-black">
                    Target gene: 
                </span>
            HTML
        );
    }
}