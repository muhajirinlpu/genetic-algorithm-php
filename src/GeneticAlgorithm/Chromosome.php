<?php

namespace App\GeneticAlgorithm;

class Chromosome
{
    public int $fitness = 999;

    public function __construct(
        public Population $population,
        public string $gene,
    ) {
        $this->fitness = $this->calculateFitness($gene);
    }

    /**
     * @throws \Exception
     */
    public function mate(Chromosome $mate): array
    {
        $pivot = random_int(0, strlen($this->gene) - 2);
        
        $length = strlen($mate->gene);

        $gene1 = substr($this->gene, 0, $pivot) . substr($mate->gene, -$length + $pivot);
        $gene2 = substr($mate->gene, 0, $pivot) . substr($this->gene, -$length + $pivot);

        return [
            new self($this->population, $gene1),
            new self($this->population, $gene2)
        ];
    }

    public function mutate(): Chromosome
    {
        $gene = $this->gene;
        $gene[random_int(0, strlen($gene) - 1)] = chr(random_int(32, 122));

        return new self($this->population, $gene);
    }

    public function calculateFitness($gene): int
    {
        $fitness = 0;
        foreach (str_split($gene) as $k => $ch) {
            $fitness += abs(ord($ch) - ord($this->population->targetGene[$k]));
        }
        
        return $fitness;
    }

    public static function genRandom(Population $population): Chromosome
    {
        $i = strlen($population->targetGene);
        $chars = range(' ', 'z');
        shuffle($chars);
        $output = substr(implode($chars), 0, $i);

        return new self($population, $output);
    }

    public function __toString()
    {
        return (string) $this->fitness;
    }
}
