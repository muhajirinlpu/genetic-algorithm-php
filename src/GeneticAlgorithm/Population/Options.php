<?php

namespace App\GeneticAlgorithm\Population;

final class Options
{
    public function __construct(
        public int|float $size = 1024,
        public float $crossover = .8,
        public float $elitism = 0.1,
        public float $mutation = 0.03,
    ) {}
}