<?php

namespace App\GeneticAlgorithm;

class Population
{
    private int $tournamentSize = 3;

    public array $population = [];

    public int $maxSize;
    public int $currentSize;

    public function __construct(
        public string $targetGene,
        public ?Population\Options $options = null,
    ) {
        if (!$this->options) {
            $this->options = new Population\Options();
        }

        $this->maxSize = $this->currentSize = $this->options->size;

        while ($this->options->size--) {
            $this->population[] = Chromosome::genRandom($this);
        }

        $this->sortPopulation();
    }

    private function sortPopulation(): void
    {
        sort($this->population);
    }

    private function tournamentSelection(): Chromosome
    {
        $best = $this->population[random_int(0, $this->currentSize - 1)];

        foreach (range(0, $this->tournamentSize) as $i) {
            $cont = $this->population[random_int(0, $this->currentSize-1)];
            if ($cont->fitness < $best->fitness) {
                $best = $cont;
            }
        }

        return $best;
    }

    private function selectParents(): array
    {
        return [$this->tournamentSelection(), $this->tournamentSelection()];
    }

    private function should(float $what): bool
    {
        return (random_int(0, 1000) / 1000) < $what;
    }

    public function evolve(): void
    {
        $elite = $this->maxSize * $this->options->elitism;
        $this->population = array_slice($this->population, 0, $elite);
        $this->currentSize = count($this->population);
        while ($this->currentSize < $this->maxSize) {
            if ($this->should($this->options->crossover)) {
                [$p1, $p2] = $this->selectParents();
                $children = $p1->mate($p2);
                foreach ($children as $c) {
                    $this->population[] = $this->should($this->options->mutation) ? $c->mutate() : $c;
                    $this->currentSize++;
                }
                continue;
            }
            $this->population[] = $this->should($this->options->mutation)
                ? $this->population[$this->currentSize - 1]->mutate()
                : $this->population[$this->currentSize - 1];
            $this->currentSize++;
        }
        $this->sortPopulation();
    }

}
