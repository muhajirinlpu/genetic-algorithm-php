<?php

require_once './vendor/autoload.php';

use App\GeneticAlgorithm;

$geneticAlgorithm = new GeneticAlgorithm();

$geneticAlgorithm->run();